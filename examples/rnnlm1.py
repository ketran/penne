"""
Deep recurrent language model. This one stacks RNNs using the
recurrent.Stack class, which is analogous to composition of
finite-state transducers.
"""

import sys
sys.path.append("..")
from penne import *
from penne import lm
from penne import recurrent
import numpy
import random
import time
from six.moves import range

hidden_dims = 100
depth = 2

#train = lm.read_data("../data/inferno.en")
train = lm.read_data("../data/ptb.train.txt")
vocab = lm.make_vocab(train)
numberizer = lm.Numberizer(vocab)

#valid = lm.read_data("../data/purgatorio.en")
valid = lm.read_data("../data/ptb.valid.txt")
#test = lm.read_data("../data/paradiso.en")
test = lm.read_data("../data/ptb.test.txt")

layers = [recurrent.Map(Layer(-len(vocab), hidden_dims, f=None, bias=None))]
for i in range(depth):
    layers.append(recurrent.LSTM(hidden_dims, hidden_dims))
    #layers.append(recurrent.Simple(hidden_dims, hidden_dims))
    layers.append(recurrent.Map(Dropout(0.3)))
layers.append(recurrent.Map(Layer(hidden_dims, len(vocab), f=logsoftmax)))
rnn = recurrent.Stack(*layers)

def make_network(csent):
    csent = map(numberizer.numberize, csent)
    isent = [numberizer.numberize("<s>")] + csent[:-1]
    osent = rnn.transduce(isent)
    loss = constant(0.)
    for o, c in zip(osent, csent):
        loss -= o[c]
    return loss

trainer = SGD(learning_rate=0.1, clip_gradients=5.)
#trainer = Adagrad(learning_rate=0.1)

for epoch in range(100):
    start_time = time.time()
    random.shuffle(train)

    Dropout.enable()
    train_loss = 0.
    train_size = 0
    for words in train:
        loss = make_network(words)
        l = trainer.receive(loss)
        train_loss += l
        train_size += len(words)
    train_ppl = numpy.exp(train_loss/train_size)

    Dropout.disable()
    valid_loss = 0.
    valid_size = 0
    for words in valid:
        loss = make_network(words)
        valid_loss += compute_value(loss)
        valid_size += len(words)
    valid_ppl = numpy.exp(valid_loss/valid_size)

    epoch_time = time.time() - start_time
    print("epoch={} time={} speed={} train={} valid={}".format(epoch, epoch_time, (train_size+valid_size)/epoch_time, train_ppl, valid_ppl))
    sys.stdout.flush()

test_loss = 0.
test_size = 0
for words in test:
    loss = make_network(words)
    test_loss += compute_value(loss)
    test_size += len(words)
test_ppl = numpy.exp(test_loss/test_size)
print("test={}".format(test_ppl))
