"""
Deep recurrent language model. This is similar to rnnlm1.py, but uses minibatches.
"""

import sys
sys.path.append("..")
from penne import *
from penne import lm
from penne import recurrent
import numpy
import random
import time
from six.moves import range

hidden_dims = 100
depth = 2

#train = lm.read_data("../data/inferno.en")
train = lm.read_data("../data/ptb.train.txt")
vocab = lm.make_vocab(train)
numberizer = lm.Numberizer(vocab)

#valid = lm.read_data("../data/purgatorio.en")
valid = lm.read_data("../data/ptb.valid.txt")
#test = lm.read_data("../data/paradiso.en")
test = lm.read_data("../data/ptb.test.txt")

batch_size = min(16, len(train))

layers = [recurrent.Map(Layer(-len(vocab), hidden_dims, f=None, bias=None))]
for i in range(depth):
    layers.append(recurrent.LSTM(hidden_dims, hidden_dims))
    layers.append(recurrent.Map(Dropout(0.3)))
layers.append(recurrent.Map(Layer(hidden_dims, len(vocab), f=logsoftmax)))
rnn = recurrent.Stack(*layers)

def make_network(cbatch):
    cbatch = [map(numberizer.numberize, csent) for csent in cbatch]
    ibatch = [[numberizer.numberize("<s>")] + csent[:-1] for csent in cbatch]
    obatch = rnn.transduce_batch(ibatch)
    loss = constant(0.)
    for osent, csent in zip(obatch, cbatch):
        for o, c in zip(osent, csent):
            loss -= o[c]
    return loss

trainer = SGD(learning_rate=0.1, clip_gradients=5.)

for epoch in range(100):
    start_time = time.time()
    random.shuffle(train)

    Dropout.enable()
    train_loss = 0.
    train_size = 0
    for batch in lm.batches(train, batch_size):
        loss = make_network(batch) / constant(len(batch))
        train_loss += trainer.receive(loss) * len(batch)
        train_size += sum(len(words) for words in batch)
    train_ppl = numpy.exp(train_loss/train_size)

    Dropout.disable()
    valid_loss = 0.
    valid_size = 0
    for batch in lm.batches(valid, batch_size):
        loss = make_network(batch)
        valid_loss += compute_value(loss)
        valid_size += sum(len(words) for words in batch)
    valid_ppl = numpy.exp(valid_loss/valid_size)

    epoch_time = time.time() - start_time
    print("epoch={} time={} speed={} train={} valid={}".format(epoch, epoch_time, (train_size+valid_size)/epoch_time, train_ppl, valid_ppl))
    sys.stdout.flush()

test_loss = 0.
test_size = 0
for batch in lm.batches(test, batch_size):
    loss = make_network(batch)
    test_loss += compute_value(loss)
    test_size += sum(len(words) for words in batch)
test_ppl = numpy.exp(test_loss/test_size)
print("test={}".format(test_ppl))
