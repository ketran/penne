"""RNN encoder-decoder translation model (Sutskever et al., 2014)."""

import sys
sys.path.append("..")
from penne import *
from penne import lm
from penne import recurrent
import numpy
import random
import time
from six.moves import range

#use_gpu('cuda0')

# Read data

heldout_size = 100
vocab_size = 5000

def prepare_data(data):
    train = data[:-heldout_size]
    valid = data[-heldout_size:]
    vocab = lm.make_vocab(train, vocab_size)
    numberizer = lm.Numberizer(vocab)
    train = [[numberizer.numberize(w) for w in words] for words in train]
    valid = [[numberizer.numberize(w) for w in words] for words in valid]
    return train, valid, numberizer

ftrain, fvalid, fnumberizer = prepare_data(lm.read_data("../data/commedia.it"))
etrain, evalid, enumberizer = prepare_data(lm.read_data("../data/commedia.en"))
train = zip(ftrain, etrain)
valid = zip(fvalid, evalid)

# Model

hidden_dims = 100
depth = 1

## Encoder

layers = [recurrent.Map(Layer(-vocab_size, hidden_dims, f=None, bias=None))]
for i in range(depth):
    layers.append(recurrent.LSTM(hidden_dims, hidden_dims))
frnn = recurrent.Stack(*layers)

def encode(frnn, fwords):
    frnn.start()
    for f in reversed(fwords):
        frnn.step(f)
    return frnn.state()

## Decoder

layers = [recurrent.Map(Layer(-vocab_size, hidden_dims, f=None, bias=None))]
for i in range(depth):
    layers.append(recurrent.LSTM(hidden_dims, hidden_dims))
ernn = recurrent.Stack(*layers)

output_layer = Layer(hidden_dims, vocab_size, f=logsoftmax)

def decode_loss(ernn, state, ewords):
    ernn.start(state)
    l = constant(0.)
    e_prev = enumberizer.numberize("<s>")
    for e in ewords:
        o = output_layer(ernn.step(e_prev))
        l -= o[e]
        e_prev = e
    return l

def decode_greedy(ernn, state):
    stop = enumberizer.numberize("</s>")
    ewords = []
    ernn.start(state)
    values = {}
    e = enumberizer.numberize("<s>")
    while e != stop and len(ewords) < 100:
        o = output_layer(ernn.step(e))
        values = compute_values(o, values)
        e = numpy.argmax(values[o])
        ewords.append(e)
    return ewords

# Training

trainer = SGD(learning_rate=0.1, clip_gradients=5.)

for epoch in range(100):
    start_time = time.time()
    random.shuffle(train)

    train_loss = 0.
    train_n = 0
    for fwords, ewords in train:
        state = encode(frnn, fwords)
        l = decode_loss(ernn, state, ewords)
        train_loss += trainer.receive(l)
        train_n += len(ewords)

    valid_loss = 0.
    valid_n = 0
    for li, (fwords, ewords) in enumerate(valid):
        state = encode(frnn, fwords)
        l = decode_loss(ernn, state, ewords)
        valid_loss += compute_values(l)[l]
        valid_n += len(ewords)

        ehyp = decode_greedy(ernn, state)
        print("line={} src={}".format(li, " ".join(map(fnumberizer.denumberize, fwords))))
        print("line={} ref={}".format(li, " ".join(map(enumberizer.denumberize, ewords))))
        print("line={} hyp={}".format(li, " ".join(map(enumberizer.denumberize, ehyp))))

    epoch_time = time.time() - start_time
    print("epoch={} time={} speed={} train={} valid={}".format(epoch, epoch_time, (train_n+valid_n)/epoch_time, numpy.exp(train_loss/train_n), numpy.exp(valid_loss/valid_n)))
