"""
Feedforward language model.
Bengio et al., 2003. A neural probabilistic language model. JMLR 3:1137-1155.
"""

import sys
sys.path.append("..")
from penne import *
from penne import lm
import numpy
import random
import time
from six.moves import range

#use_gpu()

order = 3
embedding_dims = 100
hidden_dims = 100

#train = lm.read_data("../data/inferno.en")
train = lm.read_data("../data/ptb.train.txt")
vocab = lm.make_vocab(train)
numberizer = lm.Numberizer(vocab)

#valid = lm.read_data("../data/purgatorio.en")
valid = lm.read_data("../data/ptb.valid.txt")
#test = lm.read_data("../data/paradiso.en")
test = lm.read_data("../data/ptb.test.txt")

input_layer = Layer(-len(vocab), embedding_dims, f=None, bias=False)
hidden1_layer = Layer(embedding_dims*(order-1), hidden_dims)
hidden2_layer = Layer(hidden_dims, embedding_dims)
output_layer = Layer(embedding_dims, len(vocab))

def logprob(context):
    i = []
    for word in context:
        w = numberizer.numberize(word)
        e = input_layer(w)
        i.append(e)
    h1 = tanh(hidden1_layer(concatenate(i)))
    h2 = tanh(hidden2_layer(h1))
    return logsoftmax(output_layer(h2))

trainer = SGD(learning_rate=0.1, clip_gradients=5.)

for epoch in range(10):
    start_time = time.time()
    random.shuffle(train)

    train_loss = 0.
    train_size = 0
    for ngram in lm.ngrams(train, order):
        p = logprob(ngram[:-1])
        loss = -p[numberizer.numberize(ngram[-1])]
        train_loss += trainer.receive(loss)
        train_size += 1
    train_ppl = numpy.exp(train_loss/train_size)

    valid_loss = 0.
    valid_size = 0
    for ngram in lm.ngrams(valid, order):
        p = logprob(ngram[:-1])
        loss = -p[numberizer.numberize(ngram[-1])]
        valid_loss += compute_value(loss)
        valid_size += 1
    valid_ppl = numpy.exp(valid_loss/valid_size)

    epoch_time = time.time() - start_time
    print("epoch={} time={} speed={} train={} valid={}".format(epoch, epoch_time, (train_size+valid_size)/epoch_time, train_ppl, valid_ppl))
    sys.stdout.flush()

test_loss = 0.
test_size = 0
for ngram in lm.ngrams(test, order):
    p = logprob(ngram[:-1])
    loss = -p[numberizer.numberize(ngram[-1])]
    test_loss += compute_value(loss)
    test_size += 1
test_ppl = numpy.exp(test_loss/test_size)
print("test={}".format(test_ppl))
