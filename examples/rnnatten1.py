"""RNN attentional translation model (Bahdanau et al., 2014)."""

import sys
sys.path.append("..")
from penne import *
from penne import lm
from penne import recurrent, conv
import numpy
import random
import time
from six.moves import range

# Read data

data = zip(lm.read_data("../data/commedia.it"),
           lm.read_data("../data/commedia.en"))
random.shuffle(data)
train = data[:-100]
valid = data[-100:]

ftrain, etrain = zip(*train)
fvocab = lm.make_vocab(ftrain, 5000)
fnumberizer = lm.Numberizer(fvocab)
evocab = lm.make_vocab(etrain, 5000)
enumberizer = lm.Numberizer(evocab)

# Model

hidden_dims = 1000
embedding_dims = 620
output_dims = 500

## Encoder

fembeddings = [parameter(numpy.random.normal(0., 0.01, (embedding_dims,))) for f in fvocab]
frnn_forward = recurrent.GRU(embedding_dims, hidden_dims)
frnn_backward = recurrent.GRU(embedding_dims, hidden_dims)

def encode(fwords):
    fvectors = [fembeddings[fnumberizer.numberize(f)] for f in fwords]
    states_forward = frnn_forward.transduce(fvectors)
    states_backward = reversed(frnn_backward.transduce(reversed(fvectors)))
    states = []
    for sf, sb in zip(states_forward, states_backward):
        states.append(concatenate([sf, sb]))
    return stack(states)

## Decoder

class Decoder(recurrent.Transducer):
    def __init__(self, embedding_dims, hidden_dims, output_dims, vocab_size):

        self.embeddings = [parameter(numpy.random.normal(0., 0.01, (embedding_dims,))) for e in evocab]

        self.align_layer1 = Layer([hidden_dims, hidden_dims*2], hidden_dims)
        self.align_weights2 = parameter(numpy.random.normal(0., 0.01, (hidden_dims,)))

        self.rnn = recurrent.GRU(embedding_dims + hidden_dims*2, hidden_dims)

        self.output_layer1 = Layer([hidden_dims, embedding_dims, hidden_dims*2], output_dims*2)
        self.output_layer2 = Layer(output_dims, vocab_size, f=logsoftmax)

    def start(self, fstates):
        self.s = self.rnn.start() # to do: initialize correctly
        self.fstates = fstates
        self.alphas = []

    def step(self, e):

        # Source context vector: weighted average of source word vectors
        a = self.align_layer1(self.s, self.fstates)
        alpha = exp(logsoftmax(dot(a, self.align_weights2))) # bias?
        c = dot(alpha, fstates)
        self.alphas.append(alpha) # so caller can see

        # Target word vector
        ee = self.embeddings[e]

        # Feed both into the RNN
        self.s = self.rnn.step(concatenate([ee, c]))

        # Output layer
        t_tilde = self.output_layer1(self.s, ee, c)
        t = conv.max_pool(t_tilde, (2,))
        return self.output_layer2(t)

decoder = Decoder(embedding_dims, hidden_dims, output_dims, len(evocab))

def decode_loss(fstates, ewords):
    decoder.start(fstates)
    l = constant(0.)
    e_prev = enumberizer.numberize("<s>")
    for e in map(enumberizer.numberize, ewords):
        o = decoder.step(e_prev)
        l -= o[e]
        e_prev = e
    return l

def decode_greedy(fstates):
    ewords = []
    decoder.start(fstates)
    values = {}
    e = enumberizer.numberize("<s>")
    while e != enumberizer.numberize("</s>") and len(ewords) < 100:
        o = decoder.step(e)
        values = compute_values(o, values)
        e = numpy.argmax(values[o])
        ewords.append(e)
    return [enumberizer.w[e] for e in ewords]

# Training

trainer = SGD(learning_rate=0.1, clip_gradients=5.)

for epoch in range(100):
    start_time = time.time()
    random.shuffle(train)

    train_loss = 0.
    train_n = 0
    for fwords, ewords in train:
        fstates = encode(fwords)
        l = decode_loss(fstates, ewords)
        train_loss += trainer.receive(l)
        train_n += len(ewords)

    valid_loss = 0.
    valid_n = 0
    for li, (fwords, ewords) in enumerate(valid):
        fstates = encode(fwords)
        l = decode_loss(fstates, ewords)
        valid_loss += compute_value(l)
        valid_n += 1

        ehyp = decode_greedy(fstates)
        print("line={} src={}".format(li, " ".join(fwords)))
        print("line={} ref={}".format(li, " ".join(ewords)))
        print("line={} hyp={}".format(li, " ".join(ehyp)))

    epoch_time = time.time() - start_time
    print("epoch={} time={} speed={} train={} valid={}".format(epoch, epoch_time, (train_n+valid_n)/epoch_time, numpy.exp(train_loss/train_n), numpy.exp(valid_loss/valid_n)))
    
