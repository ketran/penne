"""Recurrent neural networks as finite-state transducers."""

# To do:
# Allow multiple inputs as with make_layer?

from six.moves import range
from .expr import *
from .nn import *
from . import backend as numpy

class Transducer(object):
    """Base class for transducers."""

    def state(self):
        return None
    def start(self, state=None):
        pass

    def transduce(self, inps):
        """Apply transducer to a sequence of input symbols."""

        self.start()
        outputs = []
        for inp in inps:
            outputs.append(self.step(inp))
        return outputs

    def transduce_batch(self, ibatch):
        # ibatch[i][j] is word j of sentence i
        self.start()
        obatch = [[] for isent in ibatch]
        for j in range(max(len(isent) for isent in ibatch)):
            inputs = [isent[j] if j < len(isent) else 0 for isent in ibatch]
            outputs = self.step(inputs)
            for i, osent in enumerate(obatch):
                osent.append(outputs[i])
        return obatch

class Map(Transducer):
    """Stateless transducer that just applies a function to every symbol."""

    def __init__(self, f):
        self.f = f
    def step(self, inp):
        return self.f(inp)

class Stack(Transducer):
    """Several stacked recurrent networks, or, the composition of several FSTs."""

    def __init__(self, *layers):
        self.layers = layers

    def state(self):
        return [layer.state() for layer in self.layers]

    def start(self, state=None):
        for i, layer in enumerate(self.layers):
            layer.start(state[i] if state is not None else None)

    def step(self, inp):
        val = inp
        for layer in self.layers:
            val = layer.step(val)
        return val

class Simple(Transducer):
    """Simple (Elman) recurrent network.

    input_dims:  Number of input units.
    output_dims: Number of output units.
    f:           Activation function (default tanh)
    """

    def __init__(self, input_dims, output_dims, f=tanh, model=parameter.all):
        dims = [input_dims, output_dims]
        self.layer = Layer(dims, output_dims, f=f, model=model)
        self.h0 = constant(numpy.zeros((output_dims,))) # or parameter?

    def state(self):
        return self.h
        
    def start(self, state=None):
        if state is None:
            state = self.h0
        self.h = state

    def step(self, inp):
        """inp can be either a vector Expression or an int """
        self.h = self.layer(inp, self.h)
        return self.h

class GRU(Transducer):
    """Gated recurrent unit.

    Cho et al., 2014. Learning phrase representations using RNN
    encoder-decoder for statistical machine translation. In
    Proc. EMNLP.
    """

    def __init__(self, input_dims, output_dims, model=parameter.all):
        dims = [input_dims, output_dims]
        self.reset_gate = Layer(dims, output_dims, f=sigmoid, model=model)
        self.update_gate = Layer(dims, output_dims, f=sigmoid, model=model)
        self.input_layer = Layer(dims, output_dims, model=model)
        self.h0 = constant(numpy.zeros((output_dims,))) # or parameter?

    def start(self, state=None):
        if state is None:
            state = self.h0
        self.h = state

    def step(self, inp):
        r = self.reset_gate(inp, self.h)
        z = self.update_gate(inp, self.h)
        h_tilde = self.input_layer(inp, r*self.h)
        self.h = (constant(1.)-z)*self.h + z*h_tilde
        return self.h

class LSTM(Transducer):
    """Long short-term memory recurrent network.

    This version is from: Alex Graves, "Generating sequences with
    recurrent neural networks." arXiv:1308.0850.

    input_dims:  Number of input units.
    output_dims: Number of output units.
    f:           Activation function (default tanh)
    """

    def __init__(self, input_dims, output_dims, model=parameter.all):
        dims = [input_dims, output_dims, "diag"]
        self.input_gate = Layer(dims, output_dims, f=sigmoid, model=model)
        #self.forget_gate = Layer(dims, output_dims, f=sigmoid, bias=5., model=model)
        self.output_gate = Layer(dims, output_dims, f=sigmoid, model=model)
        self.input_layer = Layer(dims[:-1], output_dims, f=tanh, model=model)
        self.h0 = constant(numpy.zeros((output_dims,))) # or parameter?
        self.c0 = constant(numpy.zeros((output_dims,))) # or parameter?

    def state(self):
        return (self.h, self.c)

    def start(self, state=None):
        if state is None:
            h, c = self.h0, self.c0
        else:
            h, c = state
        self.h, self.c = h, c

    def step(self, inp):
        i = self.input_gate(inp, self.h, self.c)
        #f = self.forget_gate(inp, self.h, self.c)
        f = constant(1.) - i
        self.c = f * self.c + i * self.input_layer(inp, self.h)
        o = self.output_gate(inp, self.h, self.c)
        self.h = o * tanh(self.c)
        return self.h
